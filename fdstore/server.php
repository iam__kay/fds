<?php
session_start();

// initializing variables
$username = "";
$email    = "";
$name = "";
$phonenumber ="";
$address = "";
$errors = array();
$phonee= ""; 

// connect to the database
$db = mysqli_connect('localhost', 'root', '', 'fds');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM newuser WHERE username='$username' OR email='$email' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
  }

  // register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password_1);// the password encrypting

  	$query = "INSERT INTO newuser (username, email, password) 
  			  VALUES('$username', '$email', '$password')";
  	mysqli_query($db, $query);
  	$_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now logged in";
  	header('location: cart.php');
  }
}
// LOGIN USER
if (isset($_POST['login_user'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
    array_push($errors, "Username is required");
  }
  if (empty($password)) {
    array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
    $password = md5($password);
    $query = "SELECT * FROM newuser WHERE username='$username' AND password='$password'";
    $results = mysqli_query($db, $query);
  if (mysqli_num_rows($results) == 1) {
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in";
      header('location: cart.php');
  }else {
      array_push($errors, "Wrong username/password combination");
    }
  }
}


//CHECKING OUT
$_SESSION['username'] = $username;
$_SESSION["address"] = $address;


    if (isset($_POST['makepayment'])) {
      // receive all input values from the form
      $name = mysqli_real_escape_string($db, $_POST['name']);
      $address = mysqli_real_escape_string($db, $_POST['address']);
      $phonenumber = mysqli_real_escape_string($db, $_POST['phonenumber']);
      $email = mysqli_real_escape_string($db, $_POST['email']);

  // form validation: ensure that the form is filled ...
  // then adding (array_push()) to correspond error with $errors array
      if (empty($name)) { 
          array_push($errors, "Fullname is required");  }
      if (empty($phonenumber)) { 
          array_push($errors, "Phone Number is required");  }
          if (empty($email)) { 
          array_push($errors, "Email is required");  }

      if (count($errors) == 0) {
          $query = "INSERT INTO checkout_address (name, address, phonenumber, email ) VALUES ('$name','$address', '$phonenumber', '$email')";
        

  $res=mysqli_query($db,"select id from checkout_address order by id desc limit 1");
        while($row=mysqli_fetch_array($res))
        {
          $_SESSION["order_id"]=$row["id"];
        }
?>
 <script type="text/javascript">
        alert("click ok to be transfered to PayPal");

        setTimeout(function(){
          window.location="paypal.php";
        },3000);

      </script>
<!--make all fields required in checkout-->
<?php
    mysqli_query($db, $query);
  } 
  else{
    array_push($error, "All Fields are required");
  }
}

?>


