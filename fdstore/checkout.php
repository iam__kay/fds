<?php include('server.php') ?>

<!DOCTYPE html>
<html>
<head>
  <title>Check Out</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header" style="width: 30%">
  	<h2>Check Out</h2>
     <?php  if (isset($_SESSION['username'])) : ?>
     <?php echo $_SESSION['username']; ?></strong> <?php endif ?> 

      </h5> <h6><li><a href="Cart.php?logout='1'" style="color: red;"> Logout </a></li> </h6></p>
  </div>

  <form method="post" action="checkout.php">
  	
    <?php include('errors.php'); ?>

  	<div class="input-group">
  	  <label>Full Name</label>
  	  <input type="text" name="name" value="<?php echo $name; ?>">
  	</div>
  	<div class="input-group">
  	  <label>Address</label>
  	  <input type="text" name="address" required>
  	</div>
  	<div class="input-group"> 
      <div class="input-group">
      <label>Phone Number</label>
      <input type="text" name="phonenumber" required>
    </div>
    <div class="input-group"> 
      <div class="input-group">
      <label>Email</label>
      <input type="text" name="email" required>
    </div>
    <div class="input-group"> 
    <button type="submit" class="btn" name="makepayment">Make Payment</button>
  	</div>
   </form>
</body>
</html>